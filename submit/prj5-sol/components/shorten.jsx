const React = require('react');

function Result(props) {
  let ret = null;
  if (Array.isArray(props.result)) {
    ret = props.result.map( e => <a href={e.shortUrl}> {e.shortUrl} </a> );
  }
  return ret;
}
class Shorten extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      result:'',
      resultHeader:''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  
  handleChange(event) {
    this.setState({value: event.target.value});
  }

  async handleSubmit(event) {
    event.preventDefault();
    const result = await this.props.ws.subst(this.state.value);
    this.setState({result: result, resultHeader:'Shortened Text'});
  }
    
  render() {
    return (
          <div class="container">
            <h1>Shorten Text</h1>
            <ul class="error"></ul>
            <form onSubmit={this.handleSubmit}>
              <label for="text">Text</label>
              <textarea id="text" name="input" cols="80" rows="10"
	                value={this.state.value} onChange={this.handleChange}/>
              <p>
                <button type="submit" value="Submit">Shorten Text</button>
              </p>
            </form>
            <h2>{this.state.resultHeader}</h2>
            <p class="result">
              <Result result={this.state.result}/>
	    </p>
          </div>
    )
  }
}

module.exports = Shorten;
