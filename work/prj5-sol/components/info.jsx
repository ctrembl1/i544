const React = require('react');

function Error(props) {
  if (props.error !== '') {
    return (<li>{props.error}</li>);
  }
  return null;
}

function Result(props) {
  if (props.error === '' && props.longUrl !== '') {
    return ( <dl class="result">
  	       <dt>longUrl</dt>
	       <dd>{props.longUrl}</dd>
	       <dt>shortUrl</dt>
	       <dd>{props.shortUrl}</dd>
	       <dt>count</dt>
	       <dd>{props.count}</dd>
	       <dt>isActive</dt>
	       <dd>{props.isActive.toString()}</dd>
	     </dl> );
  }
  return null;
}

class Info extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      shortUrl:'',
      longUrl:'',
      count:'',
      isActive:'',
      error:''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  
  handleChange(event) {
    this.setState({value: event.target.value, result: this.result});
  }
  
  async handleSubmit(event) {
    event.preventDefault();
    try {
      const result = await this.props.ws.info(this.state.value);
      this.setState({shortUrl: result.shortUrl,
	             longUrl: result.longUrl,
	             count: result.count,
	             isActive: result.isActive,
	             error:''});
    }
    catch (err) {
      this.setState({error:err.message});
    }
  }
  
  render() {
    return (
          <div class="container">
            <h1>URL Information</h1>
            <ul class="error"><Error error={this.state.error}/></ul>
	    <form onSubmit={this.handleSubmit}>
	      <label for="text">Text</label>
	      <input id="url" name="input" value={this.state.value} onChange={this.handleChange}/>
	    </form>
	    <h2></h2>
	    <Result error={this.state.error}
	            longUrl={this.state.longUrl}
		    shortUrl={this.state.shortUrl}
		    count={this.state.count}
		    isActive={this.state.isActive}/>
	  </div>
    )
  }
}

module.exports = Info;
